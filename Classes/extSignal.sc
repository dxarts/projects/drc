/*
This file is part of the DRC quark for SuperCollider 3 and is free software:
you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
<http://www.gnu.org/licenses/>.
*/


//---------------------------------------------------------------------
//
// 	Extension: Signal
//
//---------------------------------------------------------------------

/*
TODO:

- add to SignalBox?
*/

+ Signal {

	// analytic - Preis
	instantPower { ^this.analytic.magnitude.squared }
	// totalPower { ^this.instantPower.sum }  // total analytic power
	totalPower { ^this.squared.sum * 2 }  // equivalent to total analytic power
	// totalEnergy { ^this.totalPower * this.size / 2 }  // equivalent to FreqSpectrum: -totalEnergy
	totalEnergy { ^this.squared.sum * this.size }  // equivalent to FreqSpectrum: -totalEnergy

	/*
	TODO:
	- check & match sizes?
	- rename crossDensity?
	*/
	crossPower { |aSignal|
		^((this * aSignal).sum * 2)
	}
	crossEnergy { |aSignal|
		^((this * aSignal).sum * this.size)
	}
	crossDensity { |aSignal|  // as a scalar (amp)
		^((this * aSignal).sum / aSignal.squared.sum)
	}

	averageGroupDelay { ^((Array.series(this.size) * this.instantPower).sum / this.totalPower) }

	peakDelay { |oversample = 1, analytic = true|
		var thisSignal = this;
		var instantPowerIndex = { |aSignal| aSignal.instantPower.maxIndex };
		var realPowerIndex = { |aSignal| aSignal.squared.maxIndex };

		thisSignal = (oversample == 1).if({
			this
		}, {  // resample signal
			this.resize(this.size * oversample)
		});

		^(analytic == true).if({
			instantPowerIndex.value(thisSignal) / oversample
		}, {
			realPowerIndex.value(thisSignal) / oversample
		})
	}

	crossDelay { |aSignal, size, method = \fft, oversample = 1, analytic = true|
		var crossConv = this.partConvolve(aSignal.copy.reverse, size, method);

		crossConv.size.odd.if({  // even size returns better analytic performance
			crossConv = crossConv ++ Signal.zeroFill(1)
		});
		^(crossConv.peakDelay(oversample, analytic) - (aSignal.size - 1))
	}

	crossPhase { |aSignal|
		var analyticSignal = aSignal.analytic;
		^(analyticSignal.imag * this).sum.atan2(
			(analyticSignal.real * this).sum
		)
	}

	averageInstantPhase { ^this.analytic.averageInstantPhase }

	// TODO: migrate to instantFreq
	/*
	SignalBox -instaneousFreq --> SignalBox -instantFreq
	*/
	instantFreq { |mindb = -120.0, sampleRate|
		^this.instaneousFreq(mindb, sampleRate)
	}

	averageFreq { |mindb = -120.0, sampleRate|
		^(this.instantFreq(mindb, sampleRate) * this.instantPower).sum / this.totalPower
	}

	// gaussian smoothing windows
	/*
	TODO:
	- rename?
	- revisit window sizing & response match?
	*/
	*gaussianWindowBank { |size, freqs, alpha = 1, sampleRate|
		var wns;
		var windows;

		// sort, normalize (to 1.0) & add Nyquist for iteration
		wns = (2 * freqs.copy.sort.reverse / sampleRate).addFirst(1.0);

		// empty bank
		windows = Array.newClear(wns.size);

		// iterate
		wns.doAdjacentPairs({ |wnA, wnB, index|
			var sigma;  // reciprocal of standard deviation

			sigma = size / 2.pow(alpha + 1.0) * wnB * log2(wnA / wnB);

			windows.put(
				index,
				Signal.gaussianWindow(
					size,
					a: sigma
				)
			);
		});

		// collect DC
		// windows.put(wns.size - 1, Signal.zeroFill(size));
		windows.put(wns.size - 1, Signal.gaussianWindow(size, alpha));

		// return - reverse order: DC to Nyquist
		^windows.reverse
	}

}
