Probe {

	*expSS { |gain = 0.0, size = 8192, numOctaves = 12.0, rollOff = 1.0, sine = true, beta = 1, sampleRate = 48000|
		var scale;
		var freq1 = (sampleRate / 2).asInteger;
		var freq0 = freq1 * 2.pow(numOctaves.neg);
		var w = Array.geom(size, 1.0, 2.pow(numOctaves.neg / size)).as(Signal).reverse;  // normalized instantaneous frequency
		var rollOffSize = (size / numOctaves * rollOff).asInteger;
		var chirpBeta = 1;  // pink
		var chirp, probe, inverse;
		var lfWindow, probeWindow, inverseWindow;

		// synthesize
		chirp = sine.if({
			Signal.sineChirp(size, freq1, freq0, 0.0, chirpBeta, sampleRate)
		}, {
			Signal.cosineChirp(size, freq1, freq0, pi, chirpBeta, sampleRate)
		}).reverse;
		chirp = chirp * 2.sqrt / chirp.rgoertzel(size/4).magnitude;  // normalize nyquist/2; TBD: analytic solution

		// design windows
		/*
		TODO: catch error: beta =! 0 or 1
		*/
		lfWindow = Signal.welchWindow(2 * rollOffSize).keep(rollOffSize) ++ Signal.fill(size - rollOffSize, { 1.0 });
		probeWindow = (beta == 1).if({
			lfWindow
		}, {
			lfWindow * w.collect({ |wi| wi.pow((beta - 1).neg / 2)}).as(Signal)
		});
		inverseWindow = (beta == 0).if({
			probeWindow
		}, {
			(lfWindow * w.collect({ |wi| wi.pow((beta + 1) / 2)}).as(Signal))
		});

		// window
		probe = chirp * probeWindow;
		inverse = (chirp * inverseWindow).reverse;

		// rescale
		scale = gain.dbamp / probe.peak;
		probe = probe * scale;
		inverse = inverse * scale.reciprocal;

		// return probe & inverse
		^[ probe, inverse ]
	}

	/*
	NOTE: *linSS could be refactored to include args: sine, beta

	... whether doing so would be especially useful, is another question...
	*/
	*linSS { |gain = 0.0, size = 8192, sampleRate = 48000|
		var scale;
		var freq0 = 0, freq1 = (sampleRate / 2).asInteger;  // fullband (allpass)
		var beta = 0;  // allpass
		var probe = Signal.sineChirp(size, freq0, freq1, 0.0, beta, sampleRate);
		var cosTable = Signal.fftCosTable(size);
		var complex = probe.fft(Signal.zeroFill(size), cosTable);
		var spectrum = FreqSpectrum.newComplex(complex).allpass;
		var inverse;

		// normalize response: back to time domain
		complex = spectrum.asComplex;  // reuse complex
		probe = complex.real.ifft(complex.imag, cosTable).real;
		inverse = probe.copy.reverse;

		// rescale
		scale = gain.dbamp / probe.peak;
		probe = probe * scale;
		inverse = inverse * scale.reciprocal;

		// return probe & inverse
		^[ probe, inverse ]
	}

	*periodicPLN { |gain = 0.0, size = 8192, beta = 1, sampleRate = 48000|
		var scale;
		var spectrum = FreqSpectrum.powerLaw(size, beta).gaussianPhase;
		var cosTable = Signal.fftCosTable(size);
		var complex, complexInverse;
		var probe;
		var inverse;

		// design...
		(beta == 0).if({
			probe = Signal.periodicPLNoise(size, beta);
			inverse = probe.copy.flip;
		}, {
			complex = spectrum.asComplex;
			probe = complex.real.ifft(complex.imag, cosTable).real;
			complexInverse = complex.reciprocal;
			inverse = complexInverse.real.ifft(complexInverse.imag, cosTable).real;
		});

		// rescale
		scale = gain.dbamp / probe.peak;
		probe = probe * scale;
		inverse = inverse * scale.reciprocal;

		// return probe & inverse
		^[ probe, inverse ]
	}

}
