/*
This file is part of the DRC quark for SuperCollider 3 and is free software:
you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
<http://www.gnu.org/licenses/>.
*/


//---------------------------------------------------------------------
//
// 	Extension: FreqSpectrum
//
//---------------------------------------------------------------------


+ FreqSpectrum {

	/*
	TODO:
	- consolidate / refactor use w/ *butterModel
	- include butterworth phase!

	*/
	*butterLP { arg size, freq, order, sampleRate;
		var freqs;
		var magnitude;

		freqs = size.isPowerOfTwo.if({
			size.fftFreqs(sampleRate)
		}, {
			size.dftFreqs(sampleRate)
		});

		magnitude = (  // Butterworth response
			freqs.collect({ arg thisFreq;
				(1 + (thisFreq / freq).pow(2 * order)).sqrt
			})
		).reciprocal;

		^FreqSpectrum.new(magnitude).linearPhase
	}

	*butterHP { arg size, freq, order, sampleRate;
		var freqs;
		var magnitude;

		freqs = size.isPowerOfTwo.if({
			size.fftFreqs(sampleRate)
		}, {
			size.dftFreqs(sampleRate)
		});

		magnitude = (  // Butterworth response
			freqs.collect({ arg thisFreq;
				(1 + (freq / thisFreq).pow(2 * order)).sqrt
			})
		).reciprocal;

		^FreqSpectrum.new(magnitude).linearPhase
	}

	/*
	TODO:

	- add Butterworth prototypes to SignalBox
	- refactor to use these...
	*/
	/*
	~responseDict = [  // Genelec 8331a
		\hp, [ \freq, 45, \order, 6, \lr, true ].asDict,
		\lp, [ \freq, 37000, \order, 4, \lr, true ].asDict,
	].asDict
	*/
	*butterModel { arg size, responseDict, sampleRate;
		var freqs;
		var magnitude;

		freqs = size.isPowerOfTwo.if({
			size.fftFreqs(sampleRate)
		}, {
			size.dftFreqs(sampleRate)
		});

		magnitude = (
			responseDict[\lp][\lr].if({  // lowpass
				// Linkwitz-Riley (squared) response
				freqs.collect({ arg freq;
					(1 + (freq / responseDict[\lp][\freq]).pow(responseDict[\lp][\order]))  // half order
				})
			}, {
				// Butterworth response
				freqs.collect({ arg freq;
					(1 + (freq / responseDict[\lp][\freq]).pow(2 * responseDict[\lp][\order])).sqrt
				})
			}) * responseDict[\hp][\lr].if({  // highpass
				// Linkwitz-Riley (squared) response
				freqs.collect({ arg freq;
					(1 + (responseDict[\hp][\freq] / freq).pow(responseDict[\hp][\order]))  // half order
				})
			}, {
				// Butterworth response
				freqs.collect({ arg freq;
					(1 + (responseDict[\hp][\freq] / freq).pow(2 * responseDict[\hp][\order])).sqrt
				})
			})
		).reciprocal;

		^FreqSpectrum.new(magnitude).linearPhase
	}

	/*
	~responseDict = [  // 8331a - gaussianModel
	\hp, [ \freq, 45, \alpha, 3.5, \lr, true ].asDict,
	\lp, [ \freq, 37000, \alpha, 0.0625, \lr, true ].asDict,
	].asDict
	*/
	*gaussianModel { arg size, responseDict, sampleRate;
		var kernelDict = [
			\lp, Signal.gaussianBank(size, [responseDict[\lp][\freq]], responseDict[\lp][\alpha], sampleRate)[0],  // lowpass
			\hp, Signal.gaussianBank(size, [responseDict[\hp][\freq]], responseDict[\hp][\alpha], sampleRate)[1],  // highpass
		].asDict;
		var magDict;
		var cosTable;
		var imag = Signal.newClear(size);
		var magnitude = Array.fill(size, { 1.0 });

		// sample...
		magDict = size.isPowerOfTwo.if({
			cosTable = Signal.fftCosTable(size);
			kernelDict.collect({ |item| item.fft(imag, cosTable).magnitude })
		}, {
			kernelDict.collect({ |item| item.dft(imag).magnitude })
		});

		// ... Linkwitz-Riley?
		magDict.keysValuesChange({ |key, value|
			responseDict[key][\lr].if({
				value
			}, {
				value.sqrt
			})
		});

		// combine
		magDict.do({ |item| magnitude = item * magnitude });

		^FreqSpectrum.new(magnitude).linearPhase
	}

	// Preis
	totalPower { ^this.totalEnergy / this.size * 2 }

	// only implemented for size =  power of two
	excessDelay { |oversample = 1|  // oversampling in time domain
		var excess = FreqSpectrum.new(
			this.magnitude,
			this.phase - this.deepCopy.minimumPhase.phase
		);
		var complex = excess.asComplex;
		var cosTable = Signal.fftCosTable(this.size);

		^complex.real.ifft(complex.imag, cosTable).real.peakDelay(oversample);
	}
}
