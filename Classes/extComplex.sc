/*
This file is part of the DRC quark for SuperCollider 3 and is free software:
you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
<http://www.gnu.org/licenses/>.
*/


//---------------------------------------------------------------------
//
// 	Extension: Complex
//
//---------------------------------------------------------------------

/*
TODO:

- add to SignalBox?
- add to atk-sc3?
*/

+ Complex {

	// atk-sc3 synonyms
	active { ^this.real }
	reactive { ^this.imag }

	// analytic...
	instantPower { ^this.magnitude.squared }
	totalPower { ^this.instantPower.sum }
	totalEnergy { ^this.totalPower * this.real.size / 2 }

	averageGroupDelay {
		var instantPower = this.instantPower;
		^((Array.series(instantPower.size) * instantPower).sum / instantPower.sum)
	}

	peakDelay { |oversample = 1|
		^(oversample == 1).if({
			this.instantPower.maxIndex.asFloat  // floating point...?
		}, {
			Complex.new(  // resample signal
				this.real.resize(this.real.size * oversample),
				this.imag.resize(this.real.size * oversample),
			).instantPower.maxIndex / oversample
		})
	}

	crossDelay { |aComplex, size, method = \fft, oversample = 1|
		var aComplexReverse = Complex.new(
			aComplex.real.copy.reverse,
			aComplex.imag.copy.reverse.neg
		);
		var crossConv = Complex.new(
			this.real.partConvolve(aComplexReverse.real, size, method)
			- this.imag.partConvolve(aComplexReverse.imag, size, method),
			this.real.partConvolve(aComplexReverse.imag, size, method)
			+ this.imag.partConvolve(aComplexReverse.real, size, method),
		);
		crossConv.real.size.odd.if({  // even size returns better analytic performance
			crossConv = Complex.new(
				crossConv.real ++ Signal.zeroFill(1),
				crossConv.imag ++ Signal.zeroFill(1)
			)
		});
		^(crossConv.peakDelay(oversample) - (aComplex.real.size - 1))
	}

	averageInstantPhase {
		var phase = this.phase;
		var instantPower = this.instantPower;
		var totalPower = instantPower.sum;
		^Complex.new(
			((phase.cos * instantPower) / totalPower).sum,
			((phase.sin * instantPower) / totalPower).sum
		).phase
	}

}
