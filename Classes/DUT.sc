DUT {
	var <drc, <config;
	var synthDef;
	var <delays, <gains, <phases, <bandwidths;  // measures

	*new { |drc, config|
		^super.newCopyArgs(drc, config)
	}

	load { |type = 'tests'| ^PathName(this.drc.pathDict.dut.perform(type)).files }

	probe { ^this.load('probes')[0] }
	inverse { ^this.load('inverses')[0] }
	tests { ^PathName(this.drc.pathDict.dut.tests).folders.collect{ |folder| folder.files } }
	deconvs { ^this.load('deconvs') }
	responses { ^this.load('responses') }
	archives { ^this.load('archives') }
	filters { ^this.load('filters') }

	expSS { |gain = 0.0, size = 8192, numOctaves = 9, rollOff = 1.0, sine = true|
		var signals, type = "sine";
		var beta = 0;  // fixed beta: allpass

		sine.not.if({ type = "cosine" });

		// synthesize
		signals = Probe.expSS(gain, size, numOctaves, rollOff, sine, beta, this.drc.sampleRate);

		signals[0].write(this.drc.pathDict.dut.probes +/+ "probe.wav", "WAV", "float", this.drc.sampleRate);
		signals[1].write(this.drc.pathDict.dut.inverses +/+ "inverse.wav", "WAV", "float", this.drc.sampleRate)

	}

	linSS { |gain = 0.0, size = 8192|
		var signals;

		// synthesize
		signals = Probe.linSS(gain, size, this.drc.sampleRate);

		signals[0].write(this.drc.pathDict.dut.probes +/+ "probe.wav", "WAV", "float", this.drc.sampleRate);
		signals[1].write(this.drc.pathDict.dut.inverses +/+ "inverse.wav", "WAV", "float", this.drc.sampleRate)

	}

	periodicPLN { |gain = 0.0, size = 8192, beta = 0|
		var signals;
		// var beta = 0;  // fixed beta: allpass


		signals = Probe.periodicPLN(gain, size, beta, this.drc.sampleRate);

		signals[0].write(this.drc.pathDict.dut.probes +/+ "probe.wav", "WAV", "float", this.drc.sampleRate);
		signals[1].write(this.drc.pathDict.dut.inverses +/+ "inverse.wav", "WAV", "float", this.drc.sampleRate)

	}

	loadSynth {
		SynthDef('sweepRecSynth', { |out = 0, in = 0, playBufnum = 0, recBufnum = 1, gain = -6, dur = 1, pad = 0.1|
			var src, env;
			env = EnvGen.kr(Env([1,1,0],[dur, pad]), doneAction: 2);
			src = PlayBuf.ar(1, playBufnum) * gain.dbamp * env;
			RecordBuf.ar(SoundIn.ar(in), recBufnum, loop: 0);
			Out.ar(out, src)
		}).add;
	}

	test { |inputNum = 0, out = 0, rounds = 1, interWait = 0.5|
		var probeBuffer, recBuffer, sweepDur, recBufPath;
		var cond = Condition.new, pad = 0.1, numPLNCycles = 1, rt = 0.1;
		// get config values
		numPLNCycles = this.config.cycles ? numPLNCycles;
		rt = this.config.rt ? rt;
		// is good behavior? needed to make sure server options are reloaded if server is already running
		this.drc.server.reboot;
		this.drc.server.doWhenBooted({

			this.loadSynth;

			this.drc.server.sync;

			probeBuffer = (numPLNCycles > 1 and: this.config.type == 'periodicPLN').if({
				Buffer.read(this.drc.server, this.drc.plnCycles(this.probe, numPLNCycles).fullPath)
			}, {
				Buffer.read(this.drc.server, this.probe.fullPath)
			});

			this.drc.server.sync;

			sweepDur = probeBuffer.duration + rt;

			rounds.do{ |roundNum|

				(this.drc.pathDict.dut.tests +/+ roundNum.asString.padLeft(3, "0")).mkdir;

				recBuffer = Buffer.alloc(this.drc.server, (sweepDur * this.drc.sampleRate).asInteger, 1);

				this.drc.server.sync;

				recBufPath = this.drc.pathDict.dut.tests +/+ roundNum.asString.padLeft(3, "0") +/+ inputNum.asString.padLeft(3, "0") ++ ".wav";

				this.drc.server.sendBundle(
					interWait, [
						's_new', 'sweepRecSynth', this.drc.server.nextNodeID, 0, 1,
						'out', out,
						'in', inputNum,
						'playBufnum', probeBuffer.bufnum,
						'recBufnum', recBuffer.bufnum,
						'dur', sweepDur,
						'pad', pad
				]);

				// wait for sweep to complete
				(sweepDur + pad).wait;

				interWait.wait;

				this.drc.server.sync;

				// write to disk!
				recBuffer.write(path: recBufPath, headerFormat: "wav", sampleFormat: "float");

				this.drc.server.sync;

				recBuffer.free;

				this.drc.server.sync;

				interWait.wait;
			};

			probeBuffer.free;

			this.drc.server.sync;

			"Test Completed".postln

		});


	}

	deconv { |startFrame = 0, targetSize = nil, channels = nil, gain = nil|

		// report
		"Deconvolve: DUT".postln;

		(this.config.type == 'periodicPLN').if({
			this.deconvPeriodicPLN(startFrame, targetSize, channels, gain)
		}, {
			this.deconvSS(targetSize, channels, gain)
		})
	}

	deconvSS { |targetSize, channels, gain|
		var inverseSig, testPaths;
		var keepSize;  // target trim size
		var scale;

		scale = (gain == nil).if({ this.config.gain.neg }, { gain }).dbamp;

		#inverseSig, testPaths, keepSize = this.prDeconv(channels, targetSize);

		// iterate through tests...
		// ... deconvolve and write
		testPaths.do{ |pathName, i|

			// report
			"  channel: % / %".format(i, testPaths.size - 1).postln;

			Signal.read(pathName.fullPath)
			.convolve(inverseSig)
			.drop(inverseSig.size - 1)
			.extend(keepSize, 0.0)
			.scale(scale)
			.write(
				this.drc.pathDict.dut.deconvs +/+ pathName.fileName, "WAV", "float", this.drc.sampleRate
			)
		}

	}

	// convenience methods
	deconvExpSS { |targetSize = nil, channels = nil, gain = nil| this.deconvSS(targetSize, channels, gain) }
	deconvLinSS { |targetSize = nil, channels = nil, gain = nil| this.deconvSS(targetSize, channels, gain) }

	deconvPeriodicPLN { |startFrame = 0, targetSize = nil, channels = nil, gain = nil|
		var testPaths, periods = 1;
		var inverseSig, window, windowSize;
		var rotateN, keepSize;  // target trim size;
		var scale;

		scale = (gain == nil).if({ this.config.gain.neg }, { gain }).dbamp;

		// get periods from config
		this.config.cycles.notNil.if({ periods = this.config.cycles - 1 });
		(periods < 1).if({ periods = 1 });

		#inverseSig, testPaths, keepSize = this.prDeconv(channels, targetSize);

		// iterate through tests...
		// ... deconvolve and write
		/*
		TODO: may want to include a progress indicator for user
		*/
		windowSize = periods * this.config.testSize;
		window = (periods > 1).if({
			2 / periods * Signal.hannWindow(windowSize)
		});

		rotateN = (startFrame - (startFrame.div(this.config.testSize) * this.config.testSize));

		testPaths.do({ |pathName, i|
			var test = Signal.read(pathName.fullPath, windowSize, startFrame);

			// report
			"  channel: % / %".format(i, testPaths.size - 1).postln;

			(periods > 1).if({  // average?
				test = (window * test).clump(this.config.testSize).sum.as(Signal)  // hann
				// test = test.clump(size).sum.as(Signal) / periods  // rectangular
			});

			inverseSig
			.periodicConvolve(test)
			.rotate(rotateN)
			.extend(keepSize, 0.0)
			.scale(scale)
			.write(
				this.drc.pathDict.dut.deconvs +/+ pathName.fileName, "WAV", "float", this.drc.sampleRate
			)
		})

	}

	makeResponse {
		var windowedDeconvs, windowedResponses;
		var sampleRate = this.drc.sampleRate;
		var nyquist = sampleRate.div(2);
		var foaSize = HoaOrder.new(1).size;
		var responseSFile, responses;
		var kernelSize = 2.pow(14).asInteger;  // 16384
		var testSize = 2.pow(14).asInteger;  // 16384
		var freqSpecSize;
		var fftCosTable;

		// report
		"Making response: DUT".postln;

		// get config values
		kernelSize = this.config.kernelSize ? kernelSize;
		testSize = this.config.testSize ? testSize;

		// collect the raw deconvolutions
		windowedDeconvs = this.deconvs.collect({ |pathName, i|
			Signal.read(pathName.fullPath)
		});

		/*
		estimate gain and delay
		*/
		// measure in frequency domain
		freqSpecSize = testSize.nextPowerOfTwo;
		fftCosTable = Signal.fftCosTable(
			freqSpecSize
		);

		// report
		"  Measure: gains, delays".postln;

		# gains, delays = windowedDeconvs.collect({ |item, i|
			var itemSpec = FreqSpectrum.newComplex(
				item.zeroPad(freqSpecSize).fft(  // rescale (pressure) gain (FOA / fuma)
					Signal.zeroFill(freqSpecSize),
					fftCosTable
				)
			);

			// report
			"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

			[
				itemSpec.averageMagnitude.ampdb,
				itemSpec.excessDelay,
			]
		}).flop;

		// remove gain offset
		windowedDeconvs = windowedDeconvs * gains.neg.dbamp;

		// response window?
		this.config.window.switch(
			\rectWindow, {
				// report
				"  Window: rectangular".postln;

				// center windowedDeconvs
				// - rotate for periodic noise, shift for sine sweeps
				windowedResponses = (this.config.type == \periodicPLN).if({
					windowedDeconvs.collect({ |item, i|
						// report
						"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;
						item.deepCopy.rotate(((0.5 * item.size) - delays.at(i).round).asInteger)
					})
				}, {
					windowedDeconvs.collect({ |item, i|  // shift fills zeros
						// report
						"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;
						item.as(Array).shift(((0.5 * item.size) - delays.at(i).round).asInteger).as(Signal)
					})
				});

				// trim to target kernel size
				(kernelSize < testSize).if({
					windowedResponses = windowedResponses.collect({ |item|
						item.copyRange(
							(testSize - kernelSize).div(2),
							(testSize + kernelSize).div(2) - 1
						)
					})
				})
			},
			\minimumWindow, {
				// report
				"  Window: minimum phase".postln;

				// extract minimum phase
				// - impulse centered
				windowedResponses = windowedDeconvs.collect({ |item, i|
					// report
					"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;
					Signal.zeroFill(kernelSize.div(2)) ++ item.zeroPad(freqSpecSize).minimumPhase(oversample: 2).keep(kernelSize.div(2))
				})
			},
			\smoothWindow, {
				var freqs, kernels, kernelPowers;
				var alpha = 0.125;
				var bandsPerOctave = 9;
				var numOctaves = 11;

				// report
				"  Window: smooth magnitude & phase".postln;

				// get config values
				alpha = this.config.alpha ? alpha;
				bandsPerOctave = this.config.bandsPerOctave ? bandsPerOctave;
				numOctaves = this.config.numOctaves ? numOctaves;

				// setup kernels & kernel envelopes
				freqs = ((bandsPerOctave * numOctaves) - 1).round(1.0).asInteger.collect({ |i|
					2.pow(((i + 1) / bandsPerOctave)).reciprocal
				}) * nyquist;

				kernels = Signal.gaussianBank(freqSpecSize, freqs, alpha, sampleRate);
				kernelPowers = kernels.collect({ |item| item.squared.sum });

				// smooth and extract minimum phase
				windowedResponses = windowedDeconvs.collect({ |item, i|
					var linearItem = item.zeroPad(freqSpecSize).linearPhase;
					var scale = kernelPowers.sum.sqrt;

					// report
					"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

					// minimum phase, impulse centered
					Signal.zeroFill(kernelSize.div(2)) ++ (
						scale * (
							kernels.collect({ |ktem, k|
								((linearItem * ktem).sum / kernelPowers.at(k)).sqrt * ktem
							})
						).sum.minimumPhase(oversample: 2).keep(kernelSize.div(2))
					)
				})
			},
			{  // not implemented!
				Error.new("Windowing via %, not implemented!".format(this.config.window)).throw
			}
		);

		// report
		"  Measure: phase, bandwidth".postln;

		// measure phase & bandwidth
		# phases, bandwidths = windowedResponses.collect({ |item, i|
			var cosTable = Signal.fftCosTable(item.size);

			// report
			"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

			[
				item.averageInstantPhase.raddeg,
				// normalized equivalent bandwidth
				FreqSpectrum.newComplex(item.fft(Signal.zeroFill(item.size), cosTable)).bandwidth(sampleRate: 2.0)
			]
		}).flop;

		/*
		write to disk: windowedResponses - as 4 (foaSize) channel files
		*/
		// design target paths
		responses = PathName.new(this.drc.pathDict.dut.responses +/+ "response.wav");

		/*
		TODO: refactor this block for re-use? (see: LST:-deconvPeriodicPLN)
		*/
		/*
		TODO: consider refactoring so all pathName vars are clearly pathNames!!
		*/
		responseSFile = SoundFile.openWrite(
			responses.fullPath,  // responses is just a single pathName
			"WAV",
			"float",
			foaSize,  // numChannels
			this.drc.sampleRate
		);

		// write...
		responseSFile.writeData(
			windowedResponses.collect({ |item|
				item.as(Array)  // convert to Array
			}).lace.as(FloatArray)  // interleave & FloatArray
		);
		responseSFile.close;

		// ... and archive
		delays.writeArchive(this.drc.pathDict.dut.archives +/+ "delays.scd");
		gains.writeArchive(this.drc.pathDict.dut.archives +/+ "gains.scd");
		phases.writeArchive(this.drc.pathDict.dut.archives +/+ "phases.scd");
		bandwidths.writeArchive(this.drc.pathDict.dut.archives +/+ "bandwidths.scd")
	}

	/*
	TODO:

	- remove loop as we'll just have one (multichannel) DUT response to correct?
	- more arguments for further windowing &/or filter design?
	*/
	makeFilter {
		var regDb = -120.0;
		var reg;
		var filterName = "filter.wav", filters;

		// report
		"Making filter: DUT".postln;

		// get config values
		regDb = this.config.regDb ? regDb;  // use same regularisation as analysis

		reg = regDb.dbamp;

		// design target paths
		filters = this.responses.collect({ |pathName|
			PathName.new(
				pathName.fullPath
				.replace("responses", "filters")
			)
		});

		/*
		TODO:

		-combine w/ above loop?
		*/
		// design correction
		this.responses.do({ |pathName, i|
			var responseSFile, filterSFile;
			var size;
			var cosTable;
			var zeros;
			var inverseFilters;

			responseSFile = SoundFile.openRead(
				pathName.fullPath
			);
			size = responseSFile.numFrames;
			zeros = Signal.zeroFill(size);  // zeros for imag
			cosTable = Signal.fftCosTable(size);

			/*
			TODO: consider adding header format to config file
			*/
			filterSFile = SoundFile.openWrite(
				filters.at(i).pathOnly +/+ filterName,
				"WAV",
				"float",
				responseSFile.numChannels,  // numChannels
				this.drc.sampleRate
			);

			// report
			"  Design: inverse filter".postln;

			// iterate through channels
			inverseFilters = responseSFile.numChannels.collect({ |channel|
				var response = Signal.read(
					pathName.fullPath,
					channel: channel
				);
				var complex = response.fft(zeros, cosTable);
				var compleConj = complex.conjugate;
				var inverseComplex = compleConj / (complex * compleConj + reg);

				// report
				"    channel: % / %".format(channel, responseSFile.numChannels - 1).postln;

				// IFFT, back to time domain
				inverseComplex.real.ifft(inverseComplex.imag, cosTable).real
			});

			// write...
			filterSFile.writeData(
				inverseFilters.collect({ |item|
					item.as(Array)  // convert to Array
				}).lace.as(FloatArray)  // interleave & FloatArray
			);
			responseSFile.close;
			filterSFile.close;
		})
	}

	checkValues { |type = 'delays'|
		var methods = IdentityDictionary.new.putPairs([
			'delays', "delays",
			'gains', "gains",
			'phases', "phases",
			'bandwidths', "bandwidths"
		]);
		this.perform(type).isNil.if({
			// check if archived
			File.exists(this.drc.pathDict.dut.archives +/+ type ++ ".scd").if({  // archive exists - load
				^Object.readArchive(this.drc.pathDict.dut.archives +/+ type ++ ".scd")
			}, {  // doesn't - throw error
				Error.new("DUT " ++ type ++ " unavailable! Evaluate DUT:-" ++ methods[type] ++ " to calculate.").throw;
			})
		}, {
			^this.perform(type)
		});
	}

	// convenience methods
	getPathName { |name, mri = 'tests', round| ^this.drc.getPathName(name, 'dut', mri) }
	getSignal { |name, mri = 'tests', round|  ^this.drc.getSignal(name, 'dut', mri) }
	getInverseSignal { |name| ^this.drc.getSignal('dut') }
	getTestSignal { |name, round| ^this.drc.getTestSignal(name, 'dut', round) }
	getTestPaths { |channels, round| ^this.drc.getTestPaths(channels, 'dut', round) }
	getTestSignals { |channels, round| ^this.drc.getTestSignals(channels, 'dut', round) }
	// getResponsePaths { |name, channels| this.drc.getResponsePaths(name, channels, 'dut') }
	// getResponseSignals { |name, channels| ^this.drc.getResponseSignals(name, channels, 'dut') }
	prDeconv { |channels, targetSize| ^this.drc.prDeconv(channels, targetSize, 'dut') }
	// getDelays {}
	// getGains {}

}
