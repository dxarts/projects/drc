DRC {
	var <>configPath, <sampleRate, <server;
	var <config, <projectPath, <pathDict, <lst, <dut;
	var <outputs, <numInputChannels, <inputs;
	var <latency;
	var <synthDef, <>numPLNCycles = 1;
	classvar <>configFolder;

	*initClass {
		configFolder = File.realpath(DRC.filenameSymbol.asString.dirname.dirname +/+ "config")
	}

	*new { |configPath|
		^super.newCopyArgs(configPath).init
	}

	init {
		// load configuration parameters
		config = configPath.notNil.if({
			// if file exists, direct path
			File.exists(configPath).if({
				configPath.load
			}, { // else assume it's a file in the config folder
				(configFolder +/+  configPath).load
			})
			// else load default
		}, {
			(configFolder +/+  "DRCExampleConfig.scd").load
		});

		// make project folder
		projectPath = PathName(config.projectPath ?? { Platform.defaultTempDir +/+ "DRC" }).fullPath;
		pathDict = IdentityDictionary.new(know: true);
		pathDict.putPairs([
			'lst', IdentityDictionary.new(know: true),
			'dut', IdentityDictionary.new(know: true)
		]);

		// add LST and DUT folders
		pathDict.lst.put('projectPath', (projectPath +/+ "LST").mkdir);
		pathDict.dut.put('projectPath', (projectPath +/+ "DUT").mkdir);

		pathDict.keysValuesDo{ |key, dict|
			// make project folder
			// ['probes', 'inverses', 'tests', 'deconvs', 'filters', ].do{ |folder|
			// ['probes', 'inverses', 'tests', 'deconvs', 'filters', 'responses', ].do{ |folder|
			// ['probes', 'inverses', 'tests', 'deconvs', 'filters', 'responses', 'archives', ].do{ |folder|
			// ['probes', 'inverses', 'tests', 'deconvs', 'filters', 'responses', 'correctResponses', 'archives', ].do{ |folder|
			['probes', 'inverses', 'tests', 'deconvs', 'filters', 'responses', 'correctResponses', 'correctDeconvs', 'archives', ].do{ |folder|
				dict.put(folder, (dict.projectPath +/+ folder).mkdir);
			}
		};

		sampleRate = config.sampleRate ?? { 96000 };

		// stereo and 1 sub default
		outputs = config.outputs ?? { [[0, 1], [2]] };
		// mono default?
		inputs = config.inputs ?? { [1] };
		latency = config.latency ?? { 0.0.dup(config.size) };  // by default, no latency

		// prepare server
		server = Server.default;
		this.setServerSettings;

		lst = LST.new(this, config.lst);
		dut = DUT.new(this, config.dut)

	}

	makeProbes {
		// LST
		this.lst.config !? {
			// load expSS probe for LST and its params
			(this.lst.config.type == 'expSS').if({
				// |gain = 0.0, size = 8192, numOctaves = 9, rollOff = 1.0, sine = true, beta = 1|
				lst.perform(this.lst.config.type, this.lst.config.gain ? 0.0, this.lst.config.testSize ? 8192, this.lst.config.numOctaves ? 9, this.lst.config.rollOff ? 1.0, this.lst.config.sine ? true, this.lst.config.beta ? 1)
			},{ // else its periodicPLN |gain = 0.0, size = 8192, beta = 1|
				lst.perform(this.lst.config.type, this.lst.config.gain ? 0.0, this.lst.config.testSize ? 8192, this.lst.config.beta ? 1)
			})
		};

		// DUT
		this.dut.config !? {
			// load expSS probe for DUT and its params
			case
			{ this.dut.config.type == 'expSS' } {
				// |gain = 0.0, size = 8192, numOctaves = 9, rollOff = 1.0, sine = true|
				dut.perform(this.dut.config.type, this.dut.config.gain ? 0.0, this.config.testSize ? 8192, this.dut.config.numOctaves ? 9, this.dut.config.rollOff ? 1.0, this.dut.config.sine ? true)
			}
			{ this.dut.config.type == 'linSS' }{ // if its periodicPLN |gain = 0.0, size = 8192|
				dut.perform(this.dut.config.type, this.dut.config.gain ? 0.0, this.config.testSize ? 8192)
			}
			{ this.dut.config.type == 'periodicPLN' }{ // if its periodicPLN |gain = 0.0, size = 8192, beta = 0|
				dut.perform(this.dut.config.type, this.dut.config.gain ? 0.0, this.config.testSize ? 8192, this.dut.config.beta ? 0)
			}
		}
	}

	plnCycles { |path, numCycles = 1|
		var tempSignal, newPath;
		tempSignal = Signal.read(path.fullPath);
		newPath = Platform.defaultTempDir +/+ path.fileName;
		tempSignal = tempSignal.wrapExtend((numCycles * tempSignal.size));
		tempSignal.write(newPath, "WAV", "float", sampleRate);
		this.numPLNCycles_(numCycles);
		^PathName(newPath)
	}

	setServerSettings {
		numInputChannels = inputs.size;
		server.options.numOutputBusChannels_(outputs.flatten.maxItem + 1);
		server.options.numInputBusChannels_(inputs.maxItem + 1);
		server.options.sampleRate_(sampleRate);
	}

	outputs_ { |channelArray|
		outputs = channelArray;
		this.setServerSettings
	}

	inputs_ { |channelArray|
		inputs = channelArray;
		this.setServerSettings
	}

	sampleRate_ { |newSampleRate|
		sampleRate = newSampleRate;
		this.setServerSettings
	}

	getPathName { |name, type = 'lst', mri = 'tests', round = nil|
		var index, files;

		files = this.perform(type).perform(mri);

		// for now get 1 round
		(mri == 'tests').if({
			files = round.isNil.if({
				files.last // if nil get mean or last round it doesn't exist
			}, {
				files[round] // if not, get round number
			})
		});

		// detect index by name
		index = files.detectIndex({ |pathName|
			pathName.fileName == name
		});
		// protect if nothing happens
		(index == nil).if({
			Error("File % does not exist!".format(name)).throw;
		});
		^this.perform(type).perform(mri).at(index);
	}

	getSignal { |name, type = 'lst', mri = 'tests', round = nil|
		^Signal.read(this.getPathName(name, type, mri, round).fullPath)
	}
	getInverseSignal { |type = 'lst'| ^Signal.read(this.perform(type).inverse.fullPath) }
	getTestSignal { |name, type = 'lst', round = nil| ^this.getSignal(name, type, 'tests', round) }

	getTestPaths { |channels, type = 'lst', round = nil|
		var testPaths = [];

		// TODO: check average of rounds exists?!?

		// get all channels
		channels.isNil.if({
			testPaths = round.isNil.if({
				this.perform(type).tests.last  // mean or last round it doesn't exist
			}, {
				this.perform(type).tests[round]
			})
		}, {
			// user specified channels
			channels.isKindOf(Array).not.if({ channels = [channels] });
			testPaths = channels.collect{ |channel|
				//  TO-DO: handle multiple rounds
				this.getPathName(channel.asString.padLeft(3, "0"), type, 'tests', round)
			}
		});
		(testPaths.size == 0).if({  // no matching tests?
			Error("Tests do not exist!").throw;
		});
		^testPaths
	}

	getTestSignals { |channels, type = 'lst', round = nil|
		^this.getTestPaths(channels, type, round).collect{ |pathName| Signal.read(pathName.fullPath) }
	}

	/*
	TODO: ??

	-getResponsePaths
	-getResponseSignals
	-getDelays
	-getGains
	-getFilterPaths
	-getFilterSignals
	*/

	prTestMean { |type = 'lst'|
		var meanFolder = "___";
		var pathNames, fileNames;

		// start fresh: remove meanFolder, if it exists
		File.exists(this.pathDict.perform(type).tests +/+ meanFolder).if({
			File.deleteAll(this.pathDict.perform(type).tests +/+ meanFolder)
		});

		// collect test round path names & file names
		pathNames = PathName.new(this.pathDict.perform(type).tests).folders;

		(pathNames.size == 1).if({  // only one round, copy
			File.copy(
				PathName.new(this.pathDict.perform(type).tests).folders.at(0).fullPath,
				this.pathDict.perform(type).tests +/+ meanFolder
			)
		}, {  // if more than one round, average

			// make mean directory
			File.mkdir(this.pathDict.perform(type).tests +/+ meanFolder);

			// collect test round path names & file names
			fileNames = this.inputs.collect({ |num|
				num.asString.padLeft(3, "0") ++ ".wav"
			});

			// iterate through test files (channels)
			fileNames.do({ |fileName, i|
				var testSoundFile;
				var meanSoundFile;
				var testArray, meanArray;

				// iterate through rounds
				pathNames.do({ |pathName, j|

					// on the very first round, set up mean
					(j == 0).if({
						testSoundFile = SoundFile.openRead(
							pathName.fullPath +/+ fileName
						);
						meanSoundFile = SoundFile.openWrite(
							this.pathDict.perform(type).tests +/+ meanFolder +/+ fileName,
							testSoundFile.headerFormat,
							testSoundFile.sampleFormat,
							testSoundFile.numChannels,
							this.sampleRate
						);
						meanArray = FloatArray.newClear(
							testSoundFile.numFrames * testSoundFile.numChannels
						);
						testSoundFile.close; // for good measure...
					});

					testSoundFile = SoundFile.openRead(
						pathName.fullPath +/+ fileName
					);
					testArray = FloatArray.newClear(
						testSoundFile.numFrames * testSoundFile.numChannels
					);
					testSoundFile.readData(testArray);
					meanArray = meanArray + testArray;
				});

				// average (mean)
				meanArray = meanArray * pathNames.size.reciprocal;

				// write * close
				meanSoundFile.writeData(meanArray.as(FloatArray));
				meanSoundFile.close
			})
		})
	}


	prDeconv { |channels, targetSize, type = 'lst'|
		var inverse, testPaths;
		var keepSize;  // target trim size

		// make mean
		// this.perform(type).prTestMean;
		this.prTestMean(type);

		// check if inverse exists and load
		/*
		TODO: more informative error
		*/
		inverse = this.getInverseSignal(type);
		testPaths = this.getTestPaths(channels, type);

		// target size
		keepSize = (targetSize.isNil).if({ inverse.size }, { targetSize });

		^[inverse, testPaths, keepSize]

	}


}
