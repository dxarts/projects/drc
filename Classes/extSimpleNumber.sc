/*
This file is part of the DRC quark for SuperCollider 3 and is free software:
you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
<http://www.gnu.org/licenses/>.
*/


//---------------------------------------------------------------------
//
// 	Extension: SimpleNumber
//
//---------------------------------------------------------------------

/*
TODO:

- add to SignalBox?
*/

+ SimpleNumber {

	// synonyms
	amppow { ^this.squared }
	powamp { ^this.sqrt }

	dbpow { ^(2 * this).dbamp }
	powdb { ^(10 * this.log10) }

	// limiting
	softplus { |sharp = 0|
		var sharpFac;
		^(sharp == 0).if({
			(1 + this.abs.neg.exp).log + this.max(0.0)
		}, {
			sharpFac = sharp.exp;
			(this * sharpFac).softplus / sharpFac
		})
	}
	softminus { |sharp = 0|
		^this.neg.softplus(sharp).neg
	}
	softmax { |aNumber, sharp = 0|
		^((this - aNumber).softplus(sharp) + aNumber)
	}
	softmin { |aNumber, sharp = 0|
		^((this - aNumber).softminus(sharp) + aNumber)
	}
	// a soft version of -clip
	// NOTE: -softclip already in use
	softlimit { |lo, hi, sharpLo = 0, sharpHi = 0|
		^this.softmax(lo, sharpLo).softmin(hi, sharpHi)
	}
	softlimit2 { |aNumber, sharp = 0|
		var aNumberAbs = aNumber.abs;
		^this.softmax(aNumberAbs.neg, sharp).softmin(aNumberAbs, sharp)
	}

}
