/*
This file is part of the DRC quark for SuperCollider 3 and is free software:
you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
<http://www.gnu.org/licenses/>.
*/


//---------------------------------------------------------------------
//
// 	Extension: SequenceableCollection
//
//---------------------------------------------------------------------

/*
TODO:

- add to SignalBox?
*/

+ AbstractFunction {

	// respond to math operators
	amppow { ^this.composeUnaryOp('amppow') }
	powamp { ^this.composeUnaryOp('powamp') }
	dbpow { ^this.composeUnaryOp('dbpow') }
	powdb { ^this.composeUnaryOp('powdb') }

}
