LST {
	var <drc, <config;
	var <synthDef;
	var <delays, <gains;  // measures
	var <responseCrossDelays, <crossPhases, <correlations, <crossGains;
	var <correctGains, <correctDistances, <correctDirections, <correctDelayTimes;  // corrections

	*new { |drc, config|
		^super.newCopyArgs(drc, config)
	}

	load { |type = 'tests'| ^PathName(this.drc.pathDict.lst.perform(type)).files }

	probe { ^this.load('probes')[0] }
	inverse { ^this.load('inverses')[0] }
	tests { ^PathName(this.drc.pathDict.lst.tests).folders.collect{ |folder| folder.files } }
	deconvs { ^this.load('deconvs') }
	responses { ^this.load('responses') }
	correctResponses { ^this.load('correctResponses') }
	correctDeconvs { ^this.load('correctDeconvs') }
	archives { ^this.load('archives') }
	filters { ^this.load('filters') }

	expSS { |gain = 0.0, size = 8192, numOctaves = 9, rollOff = 1.0, sine = true, beta = 1|
		var signals, type = "sine";

		sine.not.if({ type = "cosine" });

		// synthesize
		signals = Probe.expSS(gain, size, numOctaves, rollOff, sine, beta, this.drc.sampleRate);

		signals[0].write(this.drc.pathDict.lst.probes +/+ "probe.wav", "WAV", "float", this.drc.sampleRate);
		signals[1].write(this.drc.pathDict.lst.inverses +/+ "inverse.wav", "WAV", "float", this.drc.sampleRate)

	}

	periodicPLN { |gain = 0.0, size = 8192, beta = 1|
		var signals;

		signals = Probe.periodicPLN(gain, size, beta, this.drc.sampleRate);

		signals[0].write(this.drc.pathDict.lst.probes +/+ "probe.wav", "WAV", "float", this.drc.sampleRate);
		signals[1].write(this.drc.pathDict.lst.inverses +/+ "inverse.wav", "WAV", "float", this.drc.sampleRate)
	}

	loadSynth {
		SynthDef('sweepRecSynth', { |out = 0, playBufnum = 0, recBufnum = 1, gain = -6, dur = 1, pad = 0.1|
			var env;
			env = EnvGen.kr(Env([1,1,0],[dur, pad]), doneAction: 2);
			RecordBuf.ar(SoundIn.ar(this.drc.inputs), recBufnum, loop: 0);
			Out.ar(out, PlayBuf.ar(1, playBufnum) * gain.dbamp * env)
		}).add;
	}

	test { |rounds = 1, initialWait = 0.1, interWait = 0.5|
		var probeBuffer, recBuffer, sweepDur, recBufPath;
		var cond = Condition.new, pad = 0.1, numPLNCycles = 1, rt = 1.0;
		// get config values
		numPLNCycles = this.config.cycles ? numPLNCycles;
		rt = this.config.rt ? rt;
		// add extra padding
		rt = rt + 0.1;
		// add dut ring time
		this.drc.dut.config.rt.notNil.if({ rt = rt + this.drc.dut.config.rt });
		// is good behavior? needed to make sure server options are reloaded if server is already running
		this.drc.server.reboot;
		this.drc.server.doWhenBooted({

			this.loadSynth;

			this.drc.server.sync;

			initialWait.wait;

			probeBuffer = (numPLNCycles > 1 and: this.config.type == 'periodicPLN').if({
				Buffer.read(this.drc.server, this.drc.plnCycles(this.probe, numPLNCycles).fullPath)
			}, {
				Buffer.read(this.drc.server, this.probe.fullPath)
			});

			this.drc.server.sync;

			sweepDur = probeBuffer.duration + rt;

			rounds.do{ |roundNum|

				(this.drc.pathDict.lst.tests +/+ roundNum.asString.padLeft(3, "0")).mkdir;

				// for now treat all channels as the same
				// TODO: treat channel groups independently
				this.drc.outputs.flatten.do{ |channel, channelNum|

					recBuffer = Buffer.alloc(this.drc.server, (sweepDur * this.drc.sampleRate).asInteger, this.drc.numInputChannels);

					this.drc.server.sync;

					recBufPath = this.drc.pathDict.lst.tests +/+ roundNum.asString.padLeft(3, "0") +/+ channelNum.asString.padLeft(3, "0") ++ ".wav";

					this.drc.server.sendBundle(
						interWait, [
							's_new', 'sweepRecSynth', this.drc.server.nextNodeID, 0, 1,
							'out', channel,
							'playBufnum', probeBuffer.bufnum,
							'recBufnum', recBuffer.bufnum,
							'dur', sweepDur,
							'pad', pad
					]);

					// note = Synth.new('sweepRecSynth', ['playBufnum', probeBuffer.bufnum, 'recBufnum', recBuffer.bufnum, 'out', channel]);

					// wait for sweep to complete
					(sweepDur + pad).wait;

					interWait.wait;

					this.drc.server.sync;

					// write to disk!
					recBuffer.write(path: recBufPath, headerFormat: "wav", sampleFormat: "float");

					this.drc.server.sync;

					recBuffer.free;

					this.drc.server.sync;

					interWait.wait

				}

			};

			probeBuffer.free;

			this.drc.server.sync;

			"Test Completed".postln;


		});


	}

	deconv { |fftSize = 8192, startFrame = 0, targetSize = nil, channels = nil|

		// report
		"Deconvolve: LST".postln;

		(this.config.type == 'periodicPLN').if({
			this.deconvPeriodicPLN(startFrame, targetSize, channels)
		}, {
			this.deconvExpSS(fftSize, targetSize, channels)
		})
	}

	/*
	Deconvolve on Server

	NOTE: server blockSize may need to be set with respect to fftsize
	*/
	deconvExpSS { |fftsize = 8192, targetSize = nil, channels = nil|
		var testPaths;
		var keepSize;
		var nrtMemSize = 2.pow(18).asInteger;  // may want to supply this as an arg?
		var inverseSFile, inverseSig;

		// check if inverse exists
		/*
		TODO: more informative error
		*/
		#inverseSig, testPaths, keepSize = this.prDeconv(channels, targetSize);

		// inverse soundfile
		inverseSFile = SoundFile.openRead(this.inverse.fullPath);
		inverseSFile.close;  // just need to read the header

		testPaths.do({ |pathName, i|
			var uniqueID;
			var oscFilePath, tmpFilePath;
			var tmpSFile, deconvSFile;
			var deconv;

			// report
			"  channel: % / %".format(i, testPaths.size - 1).postln;

			uniqueID = UniqueID.next;
			oscFilePath = PathName.tmp +/+ "temp_oscscore" ++ uniqueID;  // explicit, to allow cleanup...
			tmpFilePath = PathName.tmp +/+ "temp_deconv" ++ uniqueID;  // ... output from NRT server

			SoundFile.use(pathName.fullPath, { |testSF|
				var nrtOptions = ServerOptions.new
				.numInputBusChannels_(testSF.numChannels)
				.numOutputBusChannels_(testSF.numChannels)
				.memSize_(nrtMemSize);
				var server = Server.new(("nrt" ++ uniqueID.asString).asSymbol, options: nrtOptions);
				var synthDef =  SynthDef.new(\partConv, { |fftsize, kernelBufnum|
					Out.ar(
						0,
						PartConv.ar(
							SoundIn.ar(
								Array.series(testSF.numChannels)
							),
							fftsize,
							kernelBufnum
						)
					)
				});
				var inverseBuf = Buffer.new(server, inverseSFile.numFrames, inverseSFile.numChannels);
				var kernelBuf = Buffer.new(
					server,
					(fftsize * ((inverseSFile.numFrames / fftsize.div(2)).roundUp)).asInteger,
					inverseSFile.numChannels  // MUST be 1, actually!
				);
				var seekOffset = inverseSFile.numFrames + fftsize.div(2) - nrtOptions.blockSize - 1;
				var score = Score.new([
					[ 0.0, inverseBuf.allocReadMsg(this.inverse.fullPath), kernelBuf.allocMsg ],  // add inverseBuf & kernelBuf
					[ 0.0, [ '/b_gen', kernelBuf.bufnum, "PreparePartConv", inverseBuf.bufnum, fftsize] ],  // prepare PartConv kernelBuf
					[ 0.0, [ '/d_recv', synthDef.asBytes ] ],  // add synthDef
					[ 0.0, Synth.basicNew(\partConv, server).newMsg(
						args: [ \fftsize, fftsize, \kernelBufnum, kernelBuf.bufnum ]
					)
					],
					[ ((seekOffset + 1 + keepSize) / this.drc.sampleRate), [ \c_set, 0, 0 ] ] // finish
				]);

				// write
				score.recordNRT(
					oscFilePath: oscFilePath,
					outputFilePath: tmpFilePath,
					inputFilePath: testSF.path,
					sampleRate: this.drc.sampleRate,
					headerFormat: "WAV",
					sampleFormat: "float",
					options: nrtOptions,
					action: { |err| // finish up!

						// read
						tmpSFile = SoundFile.openRead(tmpFilePath);
						deconv = FloatArray.newClear(keepSize * tmpSFile.numChannels);
						tmpSFile.seek(seekOffset).readData(deconv);

						// write
						deconvSFile = SoundFile.openWrite(this.drc.pathDict.lst.deconvs +/+ pathName.fileName, "WAV", "float", tmpSFile.numChannels, this.drc.sampleRate);
						deconvSFile.writeData(deconv);

						// close
						tmpSFile.close;
						deconvSFile.close;

						// clean up!
						server.remove;
						File.delete(oscFilePath);  // score
						File.delete(tmpFilePath);  // temp deconv

						// completion message
						("RESULT =" + err).postln
					}
				)
			})
		})

	}

	deconvPeriodicPLN { |startFrame = 0, targetSize = nil, channels = nil|
		var testPaths, periods = 1;
		var inverseSig, window, windowSize;
		var rotateN, keepSize;

		// get periods from config
		this.config.cycles.notNil.if({ periods = this.config.cycles - 1 });
		(periods < 1).if({ periods = 1 });

		#inverseSig, testPaths, keepSize = this.prDeconv(channels, targetSize);

		// iterate through tests...
		// ... deconvolve and write
		/*
		TODO: may want to include a progress indicator for user
		*/
		windowSize = periods * this.config.testSize;
		window = (periods > 1).if({
			2 / periods * Signal.hannWindow(windowSize)
		});

		rotateN = (startFrame - (startFrame.div(this.config.testSize) * this.config.testSize));

		testPaths.do({ |pathName, i|
			var deconv = Array.new;
			var deconvSFile = SoundFile.new;

			// report
			"  channel: % / %".format(i, testPaths.size - 1).postln;

			SoundFile.use(pathName.fullPath, { |testSF|
				deconvSFile.headerFormat_("WAV").sampleFormat_("float").sampleRate_(this.drc.sampleRate).numChannels_(testSF.numChannels);

				// iterate through channels
				testSF.numChannels.do({ |channel|
					var test = Signal.read(pathName.fullPath, windowSize, startFrame, channel);

					(periods > 1).if({  // average?
						test = (window * test).clump(this.config.testSize).sum.as(Signal)  // hann
						// test = test.clump(size).sum.as(Signal) / periods  // rectangular
					});
					deconv = deconv.add(
						inverseSig.periodicConvolve(test).rotate(rotateN).extend(keepSize, 0.0).as(Array)
					)
				})
			});

			// transpose & return as FloatArray
			deconv = deconv.flop.flatten.as(FloatArray);

			// write...
			deconvSFile.openWrite(this.drc.pathDict.lst.deconvs +/+ pathName.fileName);
			deconvSFile.writeData(deconv);
			deconvSFile.close
		})

	}

	/*
	TODO:

	- rename: analyze?, makeResponse?
	*/
	makeResponse {
		this.drc.outputs.size.do({ |i|
			"Making response: LST group %".format(i).postln;
			this.makeResponseGroup(i)
		})
	}

	makeResponseGroup { |index = 0|  // output group index, e.g., mains / sattelites, subs
		var dutDelays, dutGains, dutFilter;
		var windowedDeconvs, windowedResponses;
		var sampleRate = this.drc.sampleRate;
		var latency = this.drc.latency.at(index);  // move below to get config values?
		var nyquist = sampleRate.div(2);
		var foaSize = HoaOrder.new(1).size;
		var responseSFile, responses;
		var theseDelays, theseGains;  // temporary holders
		var indices = Array.series(this.deconvs.size);  // all
		var kernelSize = 2.pow(14).asInteger;  // 16384
		var partConvSize = 2.pow(13).asInteger;
		var freqSpecSize;
		var fftCosTable;
		var sphericalDesign, decoder, encoder;
		var angularExcessDelays;

		// get config values
		indices = this.drc.outputs.at(index) ? indices;
		kernelSize = this.config.kernelSize ? kernelSize;
		partConvSize = this.config.partConvSize ? partConvSize;

		// DUT correction measures
		dutGains = this.drc.dut.checkValues(\gains);
		dutDelays = this.drc.dut.checkValues(\delays);
		dutFilter = foaSize.collect({ |channel|
			Signal.read(this.drc.dut.filters.first.fullPath, channel: channel)  // one multichannel filter
		});

		// design A-format (angular domain) decoder & encoder
		// modelling the subcardioid microphone capsules, for now
		/*
		NOTE: returns measures equivalent to Pressure weighted Potential & Kinetic Energy Mean, Ws
		*/
		/*
		TODO: include spatial oversampling parameter?

		- oversampling emphasizes multiple reflections
		*/
		// sphericalDesign = TDesign.newHoa(optimize: \energy, order: 1);
		sphericalDesign = TDesign.newHoa(optimize: \spreadE, order: 1);
		decoder = HoaMatrixDecoder.newSphericalDesign(sphericalDesign, \controlled, 1);  // decoder
		decoder = HoaMatrixDecoder.newFromMatrix(  // convert from cardioid to subcardioid
			Matrix.with(
				(decoder.matrix.asArray.flop.clumpByHoaDegree * [ 4 / 3, 2 / 3 ]).flatten(1).flop
			).mulMatrix(  // input foa
				HoaMatrixEncoder.newFormat(\fuma, 1).matrix
			),
			sphericalDesign.directions,
			1
		);
		encoder = HoaMatrixEncoder.newSphericalDesign(sphericalDesign, \controlled, 1);  // encoder
		encoder = HoaMatrixEncoder.newFromMatrix(
			HoaMatrixDecoder.newFormat(\fuma, 1).matrix.mulMatrix(  // output foa 
				Matrix.with(  // convert from cardioid to subcardioid
					(encoder.matrix.asArray.clumpByHoaDegree * [ 4 / 3, 2 / 3 ].reciprocal).flatten(1)
				)
			),
			sphericalDesign.directions,
			1
		);

		// collect the raw deconvolutions - FOA files
		windowedDeconvs = this.deconvs.at(indices).collect({ |pathName, i|
			foaSize.collect({ |j|
				Signal.read(pathName.fullPath, channel: j)
			})
		});


		// report
		"  Remove DUT: gains, responses".postln;

		// remove DUT gain offsets & response
		windowedDeconvs = windowedDeconvs.collect({ |item, i|

			// report
			"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

			item.collect({ |jtem, j|
				dutGains.at(j).neg.dbamp * jtem.partConvolve(
					dutFilter.at(j), partConvSize
				).drop(this.drc.dut.config.kernelSize.div(2)).drop((this.drc.dut.config.kernelSize.div(2) - 1).neg)
			 })
		});

		/*
		estimate gain and delay from pressure
		 - DUT and system delays are included
		*/

		// measure in frequency domain
		freqSpecSize = windowedDeconvs.first.first.size.nextPowerOfTwo;  // presume all the same size
		fftCosTable = Signal.fftCosTable(
			freqSpecSize
		);

		// report
		"  Measure: gains, delays".postln;

		// measure against (reference) pressure
		# theseGains, theseDelays = windowedDeconvs.collect({ |item, i|
			var itemSpec = FreqSpectrum.newComplex(
				(
					HoaDegree.new(0).normalisation(\fuma).reciprocal.at(0) * item.first  // rescale (pressure) gain (FOA / fuma); TODO: beam choices
				).zeroPad(freqSpecSize).fft(
					Signal.zeroFill(freqSpecSize),
					fftCosTable
				)
			);

			// report
			"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

			[
				itemSpec.averageMagnitude.ampdb,
				itemSpec.excessDelay,  // DUT and system delay, included
			]
		}).flop;

		// report
		"  Normalize: gains".postln;

		// remove gain offset
		windowedDeconvs = windowedDeconvs.collect({ |item, i|
			// report
			"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

			theseGains.at(i).neg.dbamp * item
		});

		// response window?
		/*
		NOTE: trimmed to kernelSize
		*/
		this.config.window.switch(
			\rectWindow, {
				// report
				"  Window: rectangular".postln;
				
				// center windowedDeconvs
				// - rotate for periodic noise, shift for sine sweeps
				windowedResponses = (this.config.type == \periodicPLN).if({
					windowedDeconvs.collect({ |item, i|
						// report
						"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

						item.collect({ |jtem|
							jtem.deepCopy.rotate(
								(kernelSize.div(2) - theseDelays.at(i).round.asInteger)
							).keep(kernelSize)
						})
					})
				}, {
					windowedDeconvs.collect({ |item, i|  // shift fills zeros
						// report
						"    channel: % / %".format(i, windowedDeconvs.size - 1).postln;

						item.collect({ |jtem|
							jtem.deepCopy.as(Array).shift(
								(kernelSize.div(2) - theseDelays.at(i).round.asInteger)
							).as(Signal).keep(kernelSize)
						})
					})
				})
			},
			/*
			\minimumWindow and \smoothWindow could be combined to avoid repeated code!!
			*/
			\minimumWindow, {

				// report
				"  Window: minimum phase".postln;

				// report
				"    Translate: angular domain".postln;

				// translate to angular domain
				windowedResponses = windowedDeconvs.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedDeconvs.size - 1).postln;

					decoder.matrix.mulMatrix(Matrix.with(item)).asArray.collect({ |item| item.as(Signal) })
				});

				// report
				"    Measure: excess delay".postln;

				// find excess delays in angular domain
				angularExcessDelays = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					item.collect({ |jtem, j|
						FreqSpectrum.newComplex(
							jtem.zeroPad(freqSpecSize).fft(
								Signal.zeroFill(freqSpecSize),
								fftCosTable
							)
						).excessDelay
					}) - theseDelays.at(i)
				});

				// report
				"    Measure: minimum phase".postln;

				// extract minimum phase
				windowedResponses = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					// minimum phase, impulse centered
					item.collect({ |jtem|
						Signal.zeroFill(kernelSize.div(2)) ++ jtem.zeroPad(freqSpecSize).minimumPhase(oversample: 2).keep(kernelSize.div(2))
					})
				});

				// report
				"    Normalize: excess delay".postln;

				// accommodate excess delay offsets
				windowedResponses = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					item.collect({ |jtem, j|
						jtem.as(Array).shift(angularExcessDelays.at(i).at(j).asInteger).as(Signal)
					})
				});

				// report
				"    Translate: spherical domain".postln;

				// translate to spherical domain
				windowedResponses = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					encoder.matrix.mulMatrix(Matrix.with(item)).asArray.collect({ |item| item.as(Signal) })
				})
			},
			\smoothWindow, {
				var freqs, kernels, kernelPowers;
				var alpha = 0.125;
				var bandsPerOctave = 9;
				var numOctaves = 11;

				// report
				"  Window: smooth magnitude & phase".postln;

				// get config values
				alpha = this.config.alpha ? alpha;
				bandsPerOctave = this.config.bandsPerOctave ? bandsPerOctave;
				numOctaves = this.config.numOctaves ? numOctaves;

				// setup kernels & kernel envelopes
				freqs = ((bandsPerOctave * numOctaves) - 1).round(1.0).asInteger.collect({ |i|
					2.pow(((i + 1) / bandsPerOctave)).reciprocal
				}) * nyquist;

				kernels = Signal.gaussianBank(freqSpecSize, freqs, alpha, sampleRate);  // oversampled
				kernelPowers = kernels.collect({ |item| item.squared.sum });

				// report
				"    Translate: angular domain".postln;

				// translate to angular domain
				windowedResponses = windowedDeconvs.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedDeconvs.size - 1).postln;

					decoder.matrix.mulMatrix(Matrix.with(item)).asArray.collect({ |item| item.as(Signal) })
				});

				// report
				"    Measure: excess delay".postln;

				// find excess delays in angular domain
				angularExcessDelays = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					item.collect({ |jtem, j|
						FreqSpectrum.newComplex(
							jtem.zeroPad(freqSpecSize).fft(
								Signal.zeroFill(freqSpecSize),
								fftCosTable
							)
						).excessDelay
					}) - theseDelays.at(i)
				});

				// report
				"    Smooth: minimum phase".postln;

				// smooth and extract minimum phase
				windowedResponses = windowedResponses.collect({ |item, i|
					var scale = kernelPowers.sum.sqrt;

					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					item.collect({ |jtem, j|
						var linearJtem = jtem.zeroPad(freqSpecSize).linearPhase;

						// minimum phase, impulse centered
						Signal.zeroFill(kernelSize.div(2)) ++ (
							scale * (
								kernels.collect({ |ktem, k|
									((linearJtem * ktem).sum / kernelPowers.at(k)).sqrt * ktem
								}).sum.minimumPhase(oversample: 2).keep(kernelSize.div(2))
							)
						)
					})
				});

				// report
				"    Normalize: excess delay".postln;

				// accommodate excess delay offsets
				windowedResponses = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					item.collect({ |jtem, j|
						jtem.as(Array).shift(angularExcessDelays.at(i).at(j).asInteger).as(Signal)
					})
				});

				// report
				"    Translate: spherical domain".postln;

				// translate to spherical domain
				windowedResponses = windowedResponses.collect({ |item, i|
					// report
					"      channel: % / %".format(i, windowedResponses.size - 1).postln;

					encoder.matrix.mulMatrix(Matrix.with(item)).asArray.collect({ |item| item.as(Signal) })
				})
			},
			{  // not implemented!
				Error.new("Windowing via %, not implemented!".format(this.config.window)).throw
			}
		);

		// after centered, remove DUT & system latency delays for reporting
		/*
		is used for basic rectangular windowing, minimum phase doesn't need this - only required for reporting
		*/
		theseDelays = theseDelays - dutDelays.first - (latency * sampleRate);  // reference pressure

		/*
		write to disk: windowedResponses - as 4 (foaSize) channel files
		*/
		// design target paths
		responses = this.deconvs.at(indices).collect({ |pathName|
			PathName.new(this.drc.pathDict.lst.responses +/+ pathName.fileName)
		});

		/*
		NOTE:

		we need to be careful we're matching up the windowed response we've just
		generated with the correct LST target file name!
		*/
		responses.do({ |pathName, i|
			responseSFile = SoundFile.openWrite(
				pathName.fullPath,  // responses is just a single pathName
				"WAV",
				"float",
				foaSize,  // numChannels
				this.drc.sampleRate
			);

			// write...
			responseSFile.writeData(
				windowedResponses.at(i).collect({ |item|
					item.as(Array)  // convert to Array
				}).lace.as(FloatArray)  // interleave & FloatArray
			);
			responseSFile.close
		});

		// update... delays...
		delays.isNil.if({
			// check if archived
			File.exists(this.drc.pathDict.lst.archives +/+ "delays.scd").if({  // archive exists - load
				delays = Object.readArchive(this.drc.pathDict.lst.archives +/+ "delays.scd")
			}, {  // doesn't - create
				delays = Array.fill(this.deconvs.size, { nil });
			})
		});
		indices.do({ |i, j| delays.put(i, theseDelays.at(j) )});

		// ... update... gains...
		gains.isNil.if({
			// check if archived
			File.exists(this.drc.pathDict.lst.archives +/+ "gains.scd").if({  // archive exists - load
				gains = Object.readArchive(this.drc.pathDict.lst.archives +/+ "gains.scd")
			}, {  // doesn't - create
				gains = Array.fill(this.deconvs.size, { nil });
			})
		});
		indices.do({ |i, j| gains.put(i, theseGains.at(j) )});

		// ... and archive
		delays.writeArchive(this.drc.pathDict.lst.archives +/+ "delays.scd");
		gains.writeArchive(this.drc.pathDict.lst.archives +/+ "gains.scd")
	}

	/*
	TODO:

	- make these private methods?
	*/
	estimateCorrectGains {
		// report
		"  Calculate: correction gains".postln;

		gains = this.checkValues('gains');
		// find...
		correctGains = gains.neg + gains.minItem;
		// ... and archive
		correctGains.writeArchive(this.drc.pathDict.lst.archives +/+ "correctGains.scd");
	}

	estimateCorrectDistances {
		var tofs;

		// report
		"  Calculate: (NFE) correction distances".postln;

		// estimate NFE distances with respect to envelope peak delays
		delays = this.checkValues('delays');

		// find...
		tofs = (delays / this.drc.sampleRate);  // in seconds
		correctDistances = tofs * AtkFoa.speedOfSound;  // in meters;
		// ... and archive
		correctDistances.writeArchive(this.drc.pathDict.lst.archives +/+ "correctDistances.scd");
	}

	estimateCorrectDirections {
		var hoaOrder = HoaOrder.new(1); 
		var foaSize = hoaOrder.size;
		var decimateFac, decimateSize;
		var feature = \averageIntensity;
		var aliasFreq = 3000.0;
		var pus;
		var thetaPhiTAverageA, thetaPhiPeakA, thetaPhiFunc;

		// report
		"  Measure: directions".postln;

		// get config values
		feature = this.config.feature ? feature;
		aliasFreq = this.config.aliasFreq ? aliasFreq;

		// functions
		thetaPhiTAverageA = { |pu|
			var iTA = (pu.keep(1) * pu.drop(1)).collect({ |item|
				item.sum
			});  // time average active intensity

			Cartesian.new(iTA.at(0), iTA.at(1), iTA.at(2)).asSpherical.angles
		};
		thetaPhiPeakA = { |pu|
			var complexPU = pu.collect({ |item|
				item.analytic
			});
			var i = complexPU.drop(1).collect({ |item| complexPU.at(0) * item.conjugate});  // complex intensity
			var ia;
			var sphr;
			
			i = Complex.new(i.real, i.imag);  // reshape
			ia = i.real;  // active intensity
			sphr = Cartesian.new(ia.at(0), ia.at(1), ia.at(2)).asSpherical;

			sphr.angles.collect({ |item| item.at(sphr.magnitude.maxIndex) })
		};
		thetaPhiFunc = (feature == \peakIntensity).if({
			thetaPhiPeakA
		}, {
			thetaPhiTAverageA
		});

		// read & decimate
		decimateFac = (aliasFreq / this.drc.sampleRate.div(2)).log2.floor.asInteger;  // decimate to power of two
		decimateSize = (this.config.kernelSize * 2.pow(decimateFac)).asInteger;
		pus = this.responses.collect({ |pathName|
			foaSize.collect({ |i|
				Signal.read(pathName.fullPath, channel: i).resize(decimateSize)
			})
		}) * hoaOrder.normalisation(\fuma).reciprocal;

		// analyze
		correctDirections = pus.collect({ |item, i|
			// report
			"    channel: % / %".format(i, pus.size - 1).postln;

			thetaPhiFunc.value(item)
		});

		// ... and archive
		correctDirections.writeArchive(this.drc.pathDict.lst.archives +/+ "correctDirections.scd");
	}
	/*
	TODO:

	- include DUT filter in correction?
	-- NOT for now...
	*/

	// zero phase unit impulse filters - for consistency
	makeBasicFilters {
		var unitImpulse = Signal.zeroFill(this.config.kernelSize).put(0, 1.0);

		// report
		"  Make: correction filters".postln;

		this.responses.do({ |pathName|
			unitImpulse.write(this.drc.pathDict.lst.filters +/+ pathName.fileName,
				"WAV",
				"float",
				this.drc.sampleRate
			)
		})

	}

	makeMinimumFilters {
		this.drc.outputs.size.do({ |i|
			"  Make: correction filters: LST group %".format(i).postln;
			this.makeMinimumFiltersGroup(i)
		})
	}

	/*
	equivalent to DUT:-filter

	design minimimum response filters for correction
	*/
	makeMinimumFiltersGroup { |index = 0|
		var beams, filters;
		var modelMagnitude, targetMagnitude, averageMagnitude, beamMagnitudes, correctMagnitudes;
		var kernelSize = 2.pow(14).asInteger;  // 16384
		var indices = Array.series(this.deconvs.size);  // all
		var fftSize, fftCosTable;
		var rfftSize, rfftCosTable;
		var correctKernels;
		var beamShape = \pressure;
		var gainLimit = [ -24.0, 6.0 ];
		var gainLimitSharp = 0.0;
		var lstModels = [
			// Genelec 8331a - https://www.genelec.com/8331a#section-technical-specifications
			[
				\hp, [ \freq, 45, \order, 6, \lr, true ].asDict,
				\lp, [ \freq, 37000, \order, 4, \lr, true ].asDict,
			].asDict,
			// Genelec 7350a - https://www.genelec.com/7350a#section-technical-specifications
			[
				\hp, [ \freq, 22, \order, 4, \lr, true ].asDict,
				\lp, [ \freq, 160, \order, 4, \lr, true ].asDict,
			].asDict,
		];
		var targetWeight = 1.0;

		// get config values
		indices = this.drc.outputs.at(index) ? indices;
		kernelSize = this.config.kernelSize ? kernelSize;		
		beamShape = this.config.beamShape ? beamShape;
		gainLimit = this.config.gainLimit ? gainLimit;
		gainLimitSharp = this.config.gainLimitSharp ? gainLimitSharp;
		lstModels = this.config.lstModels ? lstModels;
		targetWeight = this.config.targetWeight ? targetWeight;

		// update... if need be
		correctDirections = this.checkValues('correctDirections');

		// ---
		// beaming...

		// collect beams
		beams = this.getBeams(beamShape).at(indices);  // TODO: not especially efficient... may want to write out beams

		// ---
		// correction filters

		// fft / rfft
		fftSize = kernelSize;
		rfftSize = fftSize.div(2) + 1;
		fftCosTable = Signal.fftCosTable(fftSize);
		rfftCosTable = Signal.rfftCosTable(rfftSize);

		beamMagnitudes = beams.collect({ |beam|
			beam.rfft(rfftCosTable).magnitude.ampdb.collect({ |item|  // only positive freqs...
				item.softmax(gainLimit.first, gainLimitSharp)  // ... softlimit
			})
		});

		averageMagnitude = beamMagnitudes.mean;

		modelMagnitude = FreqSpectrum.butterModel(
			fftSize,
			lstModels.at(index),
			this.drc.sampleRate
		).magnitude.keep(rfftSize).ampdb.collect({ |item|  // only positive freqs...
			item.softmax(gainLimit.first, gainLimitSharp)  // ... softlimit
		});

		targetMagnitude = ((1 - targetWeight) * averageMagnitude) + (targetWeight * modelMagnitude);

		// design correction magnitudes...
		correctMagnitudes = beamMagnitudes.collect({ |magnitude, i|
			// report
			"    channel: % / %".format(i, beamMagnitudes.size - 1).postln;

			(targetMagnitude - magnitude).collect({ |item|
				item.softlimit(gainLimit.first, gainLimit.last, gainLimitSharp, gainLimitSharp)  // ...softlimit
			})
		});

		// ... mirror neg freqs
		correctMagnitudes = correctMagnitudes.collect({ |item|
			item.mirror1
		});

		// synthesize correction kernels
		correctKernels = correctMagnitudes.collect({ |item|
			var complex = FreqSpectrum.new(item.dbamp).linearPhase.asComplex;
			var signal = complex.real.ifft(complex.imag, fftCosTable).real;
			signal.minimumPhase(oversample: 2)
		});

		// write out kernels...
		// design target paths
		filters = this.deconvs.at(indices).collect({ |pathName|
			PathName.new(this.drc.pathDict.lst.filters +/+ pathName.fileName)
		});

		// ... and write
		filters.do({ |pathName, i|
			correctKernels.at(i).write(pathName.fullPath, "WAV", "float", this.drc.sampleRate)
		})
	}

	// & cross delay, phase, gain, & correlations
	estimateCorrectDelayTimes {
		var foaSize = HoaOrder.new(1).size;
		var beamWeights, beams;
		var beamShape = \pressure;
		var refIndex = 0;

		// report
		"  Calculate: correction delays".postln;

		// get config values
		beamShape = this.config.beamShape ? beamShape;
		refIndex = this.config.refIndex ? refIndex;

		// update... if need be
		correctDirections = this.checkValues('correctDirections');
		delays = this.checkValues('delays');

		// collect beams
		// NOTE: beams (from responses) are gain normalized
		beams = this.getBeams(beamShape);

		// report
		"    Correct: beams".postln;

		// correct beam response
		beams = this.filters.collect({ |pathName, i|
			// report
			"      channel: % / %".format(i, this.filters.size - 1).postln;
		
			beams.at(i).convolve(Signal.read(pathName.fullPath)).keep(this.config.kernelSize)
		});

		// report
		"    Measure: cross-correlation delays".postln;

		// collect cross delays against reference
		// - required for generated corrected response
		responseCrossDelays = beams.collect({ |beam, i|
			// report
			"      channel: % / %".format(i, beams.size - 1).postln;
		
			beam.crossDelay(beams.at(refIndex), analytic: false)
		});

		// report
		"    Measure: cross-correlation phases, gains".postln;

		// collect cross phases & energy densities against reference
		# crossPhases, crossGains = beams.collect({ |beam, i|
			var refBeam = beams.at(refIndex);
			var shiftBeam = beam.as(Array).shift(
				responseCrossDelays.at(i).asInteger.neg
			).as(Signal);

			// report
			"      channel: % / %".format(i, beams.size - 1).postln;
		
			[ shiftBeam.crossPhase(refBeam), shiftBeam.crossDensity(refBeam).abs.ampdb ]
		}).flop;

		// ... now find correction delay times... & correlations
		correctDelayTimes = (delays + responseCrossDelays) / this.drc.sampleRate;
		correctDelayTimes = correctDelayTimes.maxItem - correctDelayTimes;
		correlations = crossPhases.cos;

		// ... and archive
		responseCrossDelays.writeArchive(drc.pathDict.lst.archives +/+ "responseCrossDelays.scd");
		correctDelayTimes.writeArchive(drc.pathDict.lst.archives +/+ "correctDelayTimes.scd");
		crossPhases.writeArchive(drc.pathDict.lst.archives +/+ "crossPhases.scd");
		correlations.writeArchive(drc.pathDict.lst.archives +/+ "correlations.scd");
		crossGains.writeArchive(drc.pathDict.lst.archives +/+ "crossGains.scd");
	}

	// optimize correlation with respect to reference
	optimizeCorrelations {
		var updateCorrelations = false;

		// report
		"  Optimize: phase responses".postln;

		correlations.isNil.if({
			// check if archived
			File.exists(drc.pathDict.lst.archives +/+ "correlations.scd").if({  // archive exists - load
				correlations = Object.readArchive(drc.pathDict.lst.archives +/+ "correlations.scd");
				crossPhases = Object.readArchive(drc.pathDict.lst.archives +/+ "crossPhases.scd")
			}, {  // doesn't - throw error
				Error.new("LST cross correlations unavailable! Evaluate LST:-estimateCorrectDelayTimes to calculate.").throw;
			})
		});

		// for minimum phase, just invert polarity
		this.drc.lst.filters.do({ |pathName, i|
			(correlations.at(i).isNegative).if({
				updateCorrelations = true;  // set flag

				// update measures
				correlations.put(i, correlations.at(i).neg);
				crossPhases.put(i, crossPhases.at(i) - pi);

				// report
				"    Inverting phase: %".format(i).postln;
				Signal.read(pathName.fullPath).neg.write(pathName.fullPath, "WAV", "float", drc.sampleRate)
			})
		});

		// update measures
		updateCorrelations.if({
			correlations.writeArchive(drc.pathDict.lst.archives +/+ "correlations.scd");
			crossPhases.writeArchive(drc.pathDict.lst.archives +/+ "crossPhases.scd")
		})

	}

	/*
	TODO:

	- rename correctResponse arg?
	-- specifies kernel correction of responses, or not
	-- choose false for basicBalance
	*/
	makeCorrectResponses { |correctResponse = true|
		var foaSize = HoaOrder.new(1).size;

		// report
		"  Correct: responses".postln;

		// update... if need be
		responseCrossDelays = this.checkValues('responseCrossDelays');

		this.responses.do({ |pathName, i|
			var filteredResponse = foaSize.collect({ |channel|
				var signal = Signal.read(pathName.fullPath, channel: channel);

				correctResponse.if({ // filter
					signal = signal.convolve(
						Signal.read(this.filters.at(i).fullPath)
					).keep(signal.size)
				});

				signal.as(Array).shift(  // shift
					responseCrossDelays.at(i).asInteger.neg
				)  // returned as an Array
			});
			var filteredResponseSFile = SoundFile.openWrite(
				this.drc.pathDict.lst.correctResponses +/+ pathName.fileName,  // responses is just a single pathName
				"WAV",
				"float",
				foaSize,  // numChannels
				this.drc.sampleRate
			);

			// report
			"    channel: % / %".format(i, this.responses.size - 1).postln;

			// write...
			filteredResponseSFile.writeData(
				filteredResponse.lace.as(FloatArray)  // interleave & FloatArray
			);
			filteredResponseSFile.close
		})
	}

	/*
	TODO:

	- rename correctResponse arg?
	-- specifies kernel correction of deconv, or not
	-- choose false for basicBalance
	*/
	makeCorrectDeconvs { |correctResponse = true|
		var foaSize = HoaOrder.new(1).size;

		// report
		"  Correct: deconvolutions".postln;

		// update... if need be
		correctGains = this.checkValues('correctGains');
		correctDelayTimes = this.checkValues('correctDelayTimes');

		this.deconvs.do({ |pathName, i|
			var filteredDeconv = foaSize.collect({ |channel|
				var signal = correctGains.at(i).dbamp * Signal.read(pathName.fullPath, channel: channel);

				correctResponse.if({ // filter
					signal = signal.convolve(
						Signal.read(this.filters.at(i).fullPath)
					).keep(signal.size)
				});

				signal.as(Array).shift(  // shift
					(correctDelayTimes.at(i) * this.drc.sampleRate).round.asInteger
				)  // returned as an Array
			});
			var filteredDeconvSFile = SoundFile.openWrite(
				this.drc.pathDict.lst.correctDeconvs +/+ pathName.fileName,  // responses is just a single pathName
				"WAV",
				"float",
				foaSize,  // numChannels
				this.drc.sampleRate
			);

			// report
			"    channel: % / %".format(i, this.deconvs.size - 1).postln;

			// write...
			filteredDeconvSFile.writeData(
				filteredDeconv.lace.as(FloatArray)  // interleave & FloatArray
			);
			filteredDeconvSFile.close
		})
	}

	/*
	refactored: call separate methods
	*/
	basicBalance {

		"Basic Balance: LST".postln;

		this.estimateCorrectGains;

		this.estimateCorrectDistances;

		this.estimateCorrectDirections;

		this.makeBasicFilters;

		this.estimateCorrectDelayTimes;

		this.makeCorrectResponses(false);

		this.makeCorrectDeconvs(false)
	}

	minimumPhase {

		"Minimum Phase: LST".postln;

		this.estimateCorrectGains;

		this.estimateCorrectDistances;

		this.estimateCorrectDirections;

		this.makeMinimumFilters;

		this.estimateCorrectDelayTimes;

		this.optimizeCorrelations;

		this.makeCorrectResponses(true);

		this.makeCorrectDeconvs(true)
	}

	checkValues { |type = 'correctDirections'|
		var methods = IdentityDictionary.new.putPairs([
			'correctDirections', "estimateCorrectDirections",
			'delays', "response",
			'responseCrossDelays', "estimateCorrectDelayTimes",
			'correctGains', "estimateCorrectGains",
			'correctDelayTimes', "estimateCorrectDelayTimes"
		]);
		this.perform(type).isNil.if({
			// check if archived
			File.exists(this.drc.pathDict.lst.archives +/+ type ++ ".scd").if({  // archive exists - load
				^Object.readArchive(this.drc.pathDict.lst.archives +/+ type ++ ".scd")
			}, {  // doesn't - throw error
				Error.new("LST " ++ type ++ " unavailable! Evaluate LST:-" ++ methods[type] ++ " to calculate.").throw;
			})
		}, {
			^this.perform(type)
		});
	}

	/*
	TODO: should this be a private method?
	*/
	getBeams {
		var beamWeights;
		var beamShape = \pressure;

		// get config values
		beamShape = this.config.beamShape ? beamShape;

		beamWeights = (beamShape == \pressure).if({
			Array.fill(this.correctDirections.size, { HoaDegree.new(0).normalisation(\fuma).reciprocal ++ 0.0.dup(HoaDegree.new(1).size) })
		}, {
			this.correctDirections.collect({ |thetaPhi|
				HoaMatrixDecoder.newDirection(thetaPhi.at(0), thetaPhi.at(1), beamShape, 1).matrix.mulMatrix(
					HoaMatrixEncoder.newFormat(\fuma, 1).matrix
				).getRow(0)
			})
		});

		^this.responses.collect({ |pathName, i|
			var response = HoaOrder.new(1).size.collect({ |channel|
				Signal.read(pathName.fullPath, channel: channel)
			});
			(response * beamWeights.at(i)).sum;
		});
	}

	// convenience methods
	getPathName { |name, mri = 'tests', round| ^this.drc.getPathName(name, 'lst', mri, round) }
	getSignal { |name, mri = 'tests', round|  ^this.drc.getSignal(name, 'lst', mri, round) }
	getInverseSignal { |name| ^this.drc.getSignal('lst') }
	getTestSignal { |name, round| ^this.drc.getTestSignal(name, 'lst', round) }
	getTestPaths { |channels, round| ^this.drc.getTestPaths(channels, 'lst', round) }
	getTestSignals { |channels, round| ^this.drc.getTestSignals(channels, 'lst', round) }
	prDeconv { |channels, targetSize| ^this.drc.prDeconv(channels, targetSize, 'lst') }

}
