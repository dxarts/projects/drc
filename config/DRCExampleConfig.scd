/*
NOTES:

config file for

branch: topic/_refactor-lst-basic
reference source project: "/Volumes/dev/DRC/DRC117_08_04_2022/DRC117_08_04_2022_ESS_basic/"

TODO: remove on completion!
*/
/*
https://www.genelec.com/8331a#section-technical-specifications

Group delay
The latency at high frequencies from the input to the acoustic output, measured in the analog input:

Extended Phase Linearity in GLM set to OFF (top graph)
3.2 ms

Extended Phase Linearity in GLM set to ON (bottom graph)
6.9 ms

In Genelec performance graphics, the time of converting the from an electronic input signal to the acoustic output in a Genelec monitor is described by two factors – latency and group delay. The group delay factor can be read in the graphics for a specific frequency. The total frequency-specific input-to-output delay is a sum of the latency and group delay factors. To understand the significance of this total delay, consider that moving a loudspeaker away by 1 meter creates an additional delay of about 3 ms.

https://www.genelec.com/7350a#section-technical-specifications

[ Latency is not documented for 7350a. ]
*/
/*
Are we running GLM 4?

*/
IdentityDictionary.new(know: true).putPairs([

	// project
	'projectPath', "".resolveRelative,
	'sampleRate', 96000,
	// 'outputs', [[0], [24]],  // TEST
	'outputs', [(0..23), [24]],
	'inputs', [0, 1, 2, 3],
	// 'latency', [ [ 0.0032 ], [ 0.0032 + 0.0058309037900875 ] ],  // expected lst latency, in seconds - EPL: OFF
	'latency', [ [ 0.0069 ], [ 0.0069 + 0.0058309037900875 ] ],  // expected lst latency, in seconds - EPL: ON


	// DUT
	'dut', IdentityDictionary.new(know: true).putPairs([
		// synthesis
		'type', 'periodicPLN',
		'cycles', 4,
		'rt', 0.300,  // silence pad duration: 300ms - plenty of time!
		'testSize', 2.pow(15).asInteger,
		'gain', -3.0,
		'beta', 0,

		// response analysis / windowing
		// 'window', 'rectWindow',
		// 'window', 'minimumWindow',
		'window', 'smoothWindow',

		'kernelSize', 2.pow(15).asInteger,  // target response size
		// 'kernelSize', 2.pow(13).asInteger,  // target response size
		'partConvSize', 2.pow(13).asInteger,  // partition convolution size
		'alpha', 1.0,
		// 'alpha', 0.5,
		// 'alpha', 0.25,
		// 'alpha', 0.0625,
		// 'bandsPerOctave', 24,
		'bandsPerOctave', 12,
		// 'bandsPerOctave', 3,
		// 'bandsPerOctave', 1,  // for testing...
		'numOctaves', 11,
	]),

	// LST
	'lst', IdentityDictionary.new(know: true).putPairs([
		// synthesis
		'type', 'expSS',
		'rt', 1.0,  // expected room ring time
		'testSize', 2.pow(21).asInteger,
		'gain', -18.0,
		'numOctaves', 13,
		'rollOff', 1.5,
		'sine', true,
		'beta', 1,

		// response analysis / windowing
		// 'window', 'rectWindow',
		// 'window', 'minimumWindow',
		'window', 'smoothWindow',

		'kernelSize', 2.pow(15).asInteger,  // target response size
		// 'kernelSize', 2.pow(13).asInteger,  // target response size
		'partConvSize', 2.pow(13).asInteger,  // partition convolution size
		'alpha', 1.0,
		// 'alpha', 0.25,
		// 'alpha', 0.125,
		// 'alpha', 0.0625,
		// 'bandsPerOctave', 24,
		// 'bandsPerOctave', 12,
		'bandsPerOctave', 9,
		// 'bandsPerOctave', 6,
		// 'bandsPerOctave', 3,
		// 'bandsPerOctave', 1,  // for testing...
		'numOctaves', 11,

		// spatial analysis
		'feature', 'peakIntensity',
		// 'feature', 'averageIntensity',
		'aliasFreq', 5000.0,

		// cross correlation beam forming
		'refIndex', 9,  // reference loudspeaker index
		'beamShape', 'pressure',
		// 'beamShape', 'controlled',

	]),
])
